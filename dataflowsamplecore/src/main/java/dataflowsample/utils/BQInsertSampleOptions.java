package dataflowsample.utils;

import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.Validation.Required;

public interface BQInsertSampleOptions extends DataflowSampleOptions {
	@Description("Data type")
	@Required
	String getDatatype();

	void setDatatype(String value);

	@Description("BQ project name")
	@Required
	String getBqProject();

	void setBqProject(String value);

	@Description("BQ dataset name")
	@Required
	String getBqDataset();

	void setBqDataset(String value);

	@Description("BQ table name")
	@Required
	String getBqTableName();

	void setBqTableName(String value);
}
