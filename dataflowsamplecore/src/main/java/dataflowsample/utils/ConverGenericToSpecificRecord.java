package dataflowsample.utils;

import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.specific.SpecificRecord;
import org.apache.beam.sdk.transforms.DoFn;

import dataflowsample.exception.CustomException;

public class ConverGenericToSpecificRecord extends
		DoFn<GenericRecord, SpecificRecord> {
	private static final long serialVersionUID = 9715040953946759L;
	private String datatype = "";

	public ConverGenericToSpecificRecord(String datatype) {
		this.datatype = datatype;
	}

	@ProcessElement
	public void processElement(ProcessContext ctx)
			throws IllegalArgumentException {
		try {
			SpecificRecord specificRecord = (SpecificRecord) SpecificData.get()
					.deepCopy(DataflowUtils.getSchema(datatype), ctx.element());
			ctx.output(specificRecord);
		} catch (CustomException customException) {
			throw new IllegalArgumentException(customException);
		}
	}
}
