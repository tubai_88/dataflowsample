package dataflowsample.utils;

import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecord;
import org.apache.beam.sdk.transforms.DoFn;

import dataflowsample.exception.CustomException;

public class ConvertSpecificToGenericRecord extends
		DoFn<SpecificRecord, GenericRecord> {
	private static final long serialVersionUID = 5868592439798629284L;
	private String datatype = "";

	public ConvertSpecificToGenericRecord(String datatype) {
		this.datatype = datatype;
	}

	@ProcessElement
	public void processElement(ProcessContext ctx)
			throws IllegalArgumentException {
		try {
			GenericRecord genericRecord = (GenericRecord) GenericData.get()
					.deepCopy(DataflowUtils.getSchema(datatype), ctx.element());
			ctx.output(genericRecord);
		} catch (CustomException customException) {
			throw new IllegalArgumentException(customException);
		}
	}
}
