package dataflowsample.samples.groupbykey;

import org.apache.avro.specific.SpecificRecord;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;

import dataflowsample.exception.CustomException;
import dataflowsample.utils.DataflowUtils;

public class ConvertRecordToKV extends DoFn<SpecificRecord, KV<String,SpecificRecord>> {
	private static final long serialVersionUID = -3350047371499544947L;
	private String datatype = "";

	public ConvertRecordToKV(String datatype) {
		this.datatype = datatype;
	}
	
	@ProcessElement
	public void processElement(ProcessContext ctx)
			throws IllegalArgumentException {
		try {
			SpecificRecord specificRecord = ctx.element();
			String key= DataflowUtils.getProcessor(datatype).getRecordKey(specificRecord);
			ctx.output(KV.of(key, specificRecord));
		} catch (CustomException customException) {
			throw new IllegalArgumentException(customException);
		}
	}
	
}
