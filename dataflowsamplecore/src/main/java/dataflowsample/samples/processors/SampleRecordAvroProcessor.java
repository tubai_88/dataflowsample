package dataflowsample.samples.processors;

import java.util.List;
import java.util.ArrayList;

import org.apache.avro.specific.SpecificRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dataflowsample.exception.CustomException;
import dataflowsample.schema.SampleRecord;
import dataflowsample.schema.SampleRecordGroup;

public class SampleRecordAvroProcessor implements GenericAvroProcessor {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SampleRecordAvroProcessor.class);

	@Override
	public SpecificRecord convert(String dataLine) throws CustomException {
		try {
			String[] dataFields = dataLine.split(",", -1);
			SampleRecord sampleRecord = new SampleRecord();
			sampleRecord.setGroupkey(dataFields[0]);
			sampleRecord.setField1(dataFields[1]);
			sampleRecord.setField2(dataFields[2]);
			sampleRecord.setField3(dataFields[3]);
			sampleRecord.setField4(dataFields[4]);
			sampleRecord.setField5(dataFields[5]);
			sampleRecord.setField6(dataFields[6]);
			return sampleRecord;
		} catch (Exception exception) {
			throw new CustomException(exception);
		}
	}

	@Override
	public String getRecordKey(SpecificRecord specificRecord) {
		SampleRecord sampleRecord = (SampleRecord) specificRecord;
		return sampleRecord.getGroupkey().toString();
	}

	@Override
	public SpecificRecord convert(String key, Iterable<SpecificRecord> records)
			throws CustomException {
		try {
			SampleRecordGroup sampleRecordGroup = new SampleRecordGroup();
			sampleRecordGroup.setKeyValue(key);
			List<SampleRecord> sampleRecords = new ArrayList<SampleRecord>();
			records.forEach(record -> sampleRecords.add((SampleRecord) record));
			sampleRecordGroup.setSampleRecords(sampleRecords);
			return sampleRecordGroup;
		} catch (Exception exception) {
			LOGGER.error("Exception : ",exception);
			throw new CustomException(exception);
		}
	}
}
