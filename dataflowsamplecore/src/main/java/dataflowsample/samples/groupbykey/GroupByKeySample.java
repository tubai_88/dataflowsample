package dataflowsample.samples.groupbykey;

import org.apache.avro.Schema;
import org.apache.avro.file.CodecFactory;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecord;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.io.AvroIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.GroupByKey;
import org.apache.beam.sdk.transforms.ParDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dataflowsample.exception.CustomException;
import dataflowsample.utils.ConverGenericToSpecificRecord;
import dataflowsample.utils.ConvertSpecificToGenericRecord;
import dataflowsample.utils.DataflowUtils;
import dataflowsample.utils.GroupByKeySampleOptions;

public class GroupByKeySample {
	private static final Logger logger = LoggerFactory
			.getLogger(GroupByKeySample.class);
	private static GroupByKeySample groupByKeySample = null;

	private GroupByKeySample() {
	}

	public static GroupByKeySample getInstance() {
		if (groupByKeySample == null) {
			groupByKeySample = new GroupByKeySample();
		}
		return groupByKeySample;
	}

	public void convertAndGroup(String[] args) throws CustomException {
		GroupByKeySampleOptions options = PipelineOptionsFactory.fromArgs(args)
				.withValidation().as(GroupByKeySampleOptions.class);
		Pipeline pipeline = Pipeline.create(options);

		String groupDatatype = options.getDatatype()+"Group";
		Schema schema = DataflowUtils.getSchema(options.getDatatype());
		Schema groupSchema = DataflowUtils.getSchema(groupDatatype);

		logger.info("Avro group process started : Data Type - "
				+ options.getDatatype() + ", Input - " + options.getInput());

		pipeline.apply("ReadData",
				AvroIO.readGenericRecords(schema).from(options.getInput()+"/*.avro"))
				.apply("ConvertToSpecificRecord",
						ParDo.of(new ConverGenericToSpecificRecord(options
								.getDatatype())))
				.setCoder(AvroCoder.of(SpecificRecord.class, schema))
				.apply("ConvertToKV",
						ParDo.of(new ConvertRecordToKV(options.getDatatype())))
				.apply("GroupData",
						GroupByKey.<String, SpecificRecord> create())
				.apply("CreateGroupedRecord",
						ParDo.of(new PrepareGroupRecord(options.getDatatype())))
				.setCoder(AvroCoder.of(SpecificRecord.class, groupSchema))	
				.apply("ConvertToGenericRecord",
						ParDo.of(new ConvertSpecificToGenericRecord(groupDatatype)))
				.setCoder(AvroCoder.of(GenericRecord.class, groupSchema))
				.apply("WriteAvro",
						AvroIO.writeGenericRecords(groupSchema).to(options.getOutput())
								.withCodec(CodecFactory.snappyCodec())
								.withSuffix(".avro"));
		pipeline.run().waitUntilFinish();

		logger.info("GroupByKeySample Completed, Output - "+ options.getOutput());
	}
}
