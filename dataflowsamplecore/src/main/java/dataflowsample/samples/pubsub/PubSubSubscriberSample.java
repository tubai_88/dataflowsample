package dataflowsample.samples.pubsub;

import static org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED;
import static org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.avro.Schema;
import org.apache.avro.specific.SpecificRecord;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.sdk.transforms.windowing.FixedWindows;
import org.apache.beam.sdk.transforms.windowing.Window;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.bigquery.model.TableReference;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.TopicName;

import dataflowsample.exception.CustomException;
import dataflowsample.samples.avroconverter.ConvertCSVtoAvro;
import dataflowsample.samples.wordcount.WordCountSample;
import dataflowsample.utils.BigQueryAvroUtils;
import dataflowsample.utils.DataflowUtils;
import dataflowsample.utils.PubSubPublisherSampleOptions;
import dataflowsample.utils.SampleSerializableFunction;

public class PubSubSubscriberSample {
	private static Logger logger = LoggerFactory
			.getLogger(WordCountSample.class);
	private static PubSubSubscriberSample pubSubSubscriberSample = null;
	@SuppressWarnings("rawtypes")
	private static final SerializableFunction TABLE_ROW_PARSER = new SampleSerializableFunction();

	private PubSubSubscriberSample() {
	}

	public static PubSubSubscriberSample getInstance() {
		if (pubSubSubscriberSample == null) {
			logger.debug("PubSubPublisherSample object created");
			pubSubSubscriberSample = new PubSubSubscriberSample();
		}
		return pubSubSubscriberSample;
	}

	@SuppressWarnings("unchecked")
	public void startPublish(String args[]) throws CustomException {
		PubSubPublisherSampleOptions options = PipelineOptionsFactory
				.fromArgs(args).withValidation()
				.as(PubSubPublisherSampleOptions.class);
		Pipeline pipeline = Pipeline.create(options);

		try {
			logger.info("Pubsub to bigquery job: Started");
			Schema schema = DataflowUtils.getSchema(options.getDatatype());
			TableSchema tableSchema = BigQueryAvroUtils.getTableSchema(schema);
			TableReference tableReference = new TableReference();
			tableReference.setProjectId(options.getBqProject());
			tableReference.setDatasetId(options.getBqDataset());
			tableReference.setTableId(options.getBqTableName());
			
			pipeline.apply("Read PubSub message",
					PubsubIO.readStrings().fromTopic(String.format("projects/%1$s/topics/%2$s",options.getBqProject(),options.getTopic())))
					.apply(Window.into(FixedWindows.of(Duration.standardSeconds(options.getWindowSize()))))
					.apply("ConvertMessage",
							ParDo.of(new ConvertCSVtoAvro(options.getDatatype())))
					.setCoder(AvroCoder.of(SpecificRecord.class, schema))
					.apply("WriteToBQ",
							BigQueryIO.write().to(tableReference)
									.withSchema(tableSchema)
									.withCreateDisposition(CREATE_IF_NEEDED)
									.withFormatFunction(TABLE_ROW_PARSER));
			pipeline.run().waitUntilFinish();
			logger.info("Pubsub to bigquery job ended");
		} catch (Exception e) {
			throw new CustomException(e);
		}
	}
}
