package dataflowsample.utils;

import org.apache.avro.Schema;

import dataflowsample.constants.Constants;
import dataflowsample.exception.CustomException;
import dataflowsample.samples.processors.GenericAvroProcessor;
import dataflowsample.samples.processors.SampleRecordAvroProcessor;
import dataflowsample.schema.SampleRecord;
import dataflowsample.schema.SampleRecordGroup;

public class DataflowUtils {
	public static Schema getSchema(String datatype) throws CustomException {
		Schema schema = null;
		switch (datatype.toUpperCase()) {
		case Constants.SAMPLERECORD_DATATYPE:
			schema = SampleRecord.SCHEMA$;
			break;
		case Constants.SAMPLERECORDGROUP_DATATYPE:
			schema = SampleRecordGroup.SCHEMA$;
			break;
		default:
			throw new CustomException("Unsupported Datatype");
		}
		return schema;
	}
	
	public static GenericAvroProcessor getProcessor(String datatype) throws CustomException {
		GenericAvroProcessor schema = null;
		switch (datatype.toUpperCase()) {
		case Constants.SAMPLERECORD_DATATYPE:
			schema = new SampleRecordAvroProcessor();
			break;
		default:
			throw new CustomException("Unsupported Datatype");
		}
		return schema;
	}
	
	@SuppressWarnings("rawtypes")
	public static Class getClass(String datatype) throws CustomException {
		Class schema = null;
		switch (datatype.toUpperCase()) {
		case Constants.SAMPLERECORD_DATATYPE:
			schema = SampleRecord.class;
			break;
		case Constants.SAMPLERECORDGROUP_DATATYPE:
			schema = SampleRecordGroup.class;
			break;
		default:
			throw new CustomException("Unsupported Datatype");
		}
		return schema;
	}
}
