package dataflowsample.samples.groupbykey;

import org.apache.avro.specific.SpecificRecord;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dataflowsample.exception.CustomException;
import dataflowsample.utils.DataflowUtils;

public class PrepareGroupRecord extends
		DoFn<KV<String, Iterable<SpecificRecord>>, SpecificRecord> {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PrepareGroupRecord.class);
	private static final long serialVersionUID = 6004140587411668786L;
	private String datatype = "";

	public PrepareGroupRecord(String datatype) {
		this.datatype = datatype;
	}

	@ProcessElement
	public void processElement(ProcessContext ctx)
			throws IllegalArgumentException {
		try {
			KV<String, Iterable<SpecificRecord>> keyValueData = ctx.element();
			String key = keyValueData.getKey();
			Iterable<SpecificRecord> records = keyValueData.getValue();
			SpecificRecord record = DataflowUtils.getProcessor(datatype)
					.convert(key, records);
			ctx.output(record);
		} catch (CustomException customException) {
			LOGGER.error("Exception : " + customException);
			throw new IllegalArgumentException(customException);
		} catch (Exception exception) {
			LOGGER.error("Exception : ", exception);
			throw new IllegalArgumentException(exception);
		}
	}
}
