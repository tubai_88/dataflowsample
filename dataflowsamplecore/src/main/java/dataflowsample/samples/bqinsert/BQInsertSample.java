package dataflowsample.samples.bqinsert;

import static org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED;
import static org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE;

import org.apache.avro.Schema;
import org.apache.avro.specific.SpecificRecord;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.io.AvroIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.bigquery.model.TableReference;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;

import dataflowsample.exception.CustomException;
import dataflowsample.utils.BQInsertSampleOptions;
import dataflowsample.utils.BigQueryAvroUtils;
import dataflowsample.utils.ConverGenericToSpecificRecord;
import dataflowsample.utils.DataflowUtils;
import dataflowsample.utils.SampleSerializableFunction;

public class BQInsertSample {
	private static final Logger logger = LoggerFactory
			.getLogger(BQInsertSample.class);
	private static BQInsertSample bqInsertSample = null;
	@SuppressWarnings("rawtypes")
	private static final SerializableFunction TABLE_ROW_PARSER = new SampleSerializableFunction();

	private BQInsertSample() {
	}

	public static BQInsertSample getInstance() {
		if (bqInsertSample == null) {
			bqInsertSample = new BQInsertSample();
		}
		return bqInsertSample;
	}

	@SuppressWarnings("unchecked")
	public void insertToBQ(String[] args) throws CustomException {
		BQInsertSampleOptions options = PipelineOptionsFactory.fromArgs(args)
				.withValidation().as(BQInsertSampleOptions.class);
		Pipeline pipeline = Pipeline.create(options);

		Schema schema = DataflowUtils.getSchema(options.getDatatype());
		TableSchema tableSchema = BigQueryAvroUtils.getTableSchema(schema);
		TableReference tableReference = new TableReference();
		tableReference.setProjectId(options.getBqProject());
		tableReference.setDatasetId(options.getBqDataset());
		tableReference.setTableId(options.getBqTableName());

		logger.info("BQ Insert process started : Data Type - "
				+ options.getDatatype() + ", Input - " + options.getInput());
		pipeline.apply(
				"ReadData",
				AvroIO.readGenericRecords(schema).from(
						options.getInput() + "/*.avro"))
				.apply("ConvertToSpecificRecord",
						ParDo.of(new ConverGenericToSpecificRecord(options
								.getDatatype())))
				.setCoder(AvroCoder.of(SpecificRecord.class, schema))
				.apply("WriteToBQ",
						BigQueryIO.write().to(tableReference)
								.withSchema(tableSchema)
								.withWriteDisposition(WRITE_TRUNCATE)
								.withCreateDisposition(CREATE_IF_NEEDED)
								.withFormatFunction(TABLE_ROW_PARSER));

		pipeline.run().waitUntilFinish();

		logger.info("BQ Insert Completed");
	}
}
