package dataflowsample.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dataflowsample.exception.CustomException;
import dataflowsample.samples.wordcount.WordCountSample;

public class WordCountSampleMain {
	private static final Logger log = LoggerFactory
			.getLogger(WordCountSampleMain.class);

	public static void main(String[] args) throws CustomException {
		try {
			WordCountSample wordCountSample = WordCountSample.getInstance();
			wordCountSample.runWordCount(args);
		} catch (CustomException customException) {
			log.error("Exception : ", customException);
			throw customException;
		} catch (Exception exception) {
			log.error("Exception : ", exception);
			throw new CustomException(exception);
		}
	}
}
