package dataflowsample.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dataflowsample.exception.CustomException;
import dataflowsample.samples.avroconverter.AvroConverterSample;

public class AvroConverterSampleMain {
	private static final Logger log = LoggerFactory
			.getLogger(AvroConverterSampleMain.class);

	public static void main(String[] args) throws CustomException {
		try {
			AvroConverterSample avroConverterSample = AvroConverterSample
					.getInstance();
			avroConverterSample.convertToAvro(args);
		} catch (CustomException customException) {
			log.error("Exception : ", customException);
			throw customException;
		} catch (Exception exception) {
			log.error("Exception : ", exception);
			throw new CustomException(exception);
		}
	}
}
