package dataflowsample.exception;

public class CustomException extends Exception {
	private static final long serialVersionUID = 6044320505250157574L;

	public CustomException(String message) {
		super(message);
	}

	public CustomException(Exception e) {
		super(e);
	}
}
