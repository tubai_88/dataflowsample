package dataflowsample.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dataflowsample.exception.CustomException;
import dataflowsample.samples.bqinsert.BQInsertSample;

public class BigQueryInsertSampleMain {
	private static final Logger log = LoggerFactory
			.getLogger(BigQueryInsertSampleMain.class);

	public static void main(String[] args) throws CustomException {
		try {
			BQInsertSample bqInsertSample = BQInsertSample
					.getInstance();
			bqInsertSample.insertToBQ(args);
		} catch (CustomException customException) {
			log.error("Exception : ", customException);
			throw customException;
		} catch (Exception exception) {
			log.error("Exception : ", exception);
			throw new CustomException(exception);
		}
	}
}
