package dataflowsample.samples.avroconverter;

import org.apache.avro.Schema;
import org.apache.avro.file.CodecFactory;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.specific.SpecificRecord;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.io.AvroIO;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.ParDo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dataflowsample.exception.CustomException;
import dataflowsample.samples.wordcount.WordCountSample;
import dataflowsample.utils.AvroConverterSampleOptions;
import dataflowsample.utils.ConvertSpecificToGenericRecord;
import dataflowsample.utils.DataflowUtils;

public class AvroConverterSample {
	private static Logger logger = LoggerFactory
			.getLogger(WordCountSample.class);
	private static AvroConverterSample avroConverterSample = null;

	private AvroConverterSample() {
	}

	public static AvroConverterSample getInstance() {
		if (avroConverterSample == null) {
			logger.debug("WordCountSample object created");
			avroConverterSample = new AvroConverterSample();
		}
		return avroConverterSample;
	}

	public void convertToAvro(String args[]) throws CustomException {
		AvroConverterSampleOptions options = PipelineOptionsFactory
				.fromArgs(args).withValidation()
				.as(AvroConverterSampleOptions.class);
		Pipeline pipeline = Pipeline.create(options);

		Schema schema = DataflowUtils.getSchema(options.getDatatype());

		logger.info("Avro conversion started : Data Type - "
				+ options.getDatatype() + ", Input - " + options.getInput());

		pipeline.apply("ReadLines", TextIO.read().from(options.getInput()))
				.apply("ConvertToAvro",
						ParDo.of(new ConvertCSVtoAvro(options.getDatatype())))
				.setCoder(AvroCoder.of(SpecificRecord.class, schema))
				.apply("ConvertSpecificRecordToGeneric",
						ParDo.of(new ConvertSpecificToGenericRecord(options
								.getDatatype())))
				.setCoder(AvroCoder.of(GenericRecord.class, schema))
				.apply("WriteAvro",
						AvroIO.writeGenericRecords(schema)
								.to(options.getOutput())
								.withCodec(CodecFactory.snappyCodec())
								.withSuffix(".avro"));
		pipeline.run().waitUntilFinish();

		logger.info("Avro conversion ended, Output - " + options.getOutput());
	}
}
