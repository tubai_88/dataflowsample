package dataflowsample.samples.avroconverter;

import org.apache.avro.specific.SpecificRecord;
import org.apache.beam.sdk.transforms.DoFn;

import dataflowsample.exception.CustomException;
import dataflowsample.utils.DataflowUtils;

public class ConvertCSVtoAvro extends DoFn<String, SpecificRecord> {
	private static final long serialVersionUID = -4067484397147369999L;

	private String datatype = "";

	public ConvertCSVtoAvro(String datatype) {
		this.datatype = datatype;
	}

	@ProcessElement
	public void processElement(ProcessContext ctx)
			throws IllegalArgumentException {
		try {
			ctx.output(DataflowUtils.getProcessor(datatype).convert(
					ctx.element()));
		} catch (CustomException customException) {
			throw new IllegalArgumentException(customException);
		}
	}
}
