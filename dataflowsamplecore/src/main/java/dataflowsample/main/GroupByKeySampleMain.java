package dataflowsample.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dataflowsample.exception.CustomException;
import dataflowsample.samples.groupbykey.GroupByKeySample;

public class GroupByKeySampleMain {
	private static final Logger log = LoggerFactory
			.getLogger(AvroConverterSampleMain.class);

	public static void main(String[] args) throws CustomException {
		try {
			GroupByKeySample groupByKeySample = GroupByKeySample.getInstance();
			groupByKeySample.convertAndGroup(args);
		} catch (CustomException customException) {
			log.error("Exception : ", customException);
			throw customException;
		} catch (Exception exception) {
			log.error("Exception : ", exception);
			throw new CustomException(exception);
		}
	}
}
