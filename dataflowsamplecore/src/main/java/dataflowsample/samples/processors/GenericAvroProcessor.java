package dataflowsample.samples.processors;

import org.apache.avro.specific.SpecificRecord;

import dataflowsample.exception.CustomException;

public interface GenericAvroProcessor {
  public SpecificRecord convert(String dataLine) throws CustomException;
  public SpecificRecord convert(String key,Iterable<SpecificRecord> records) throws CustomException;
  public String getRecordKey(SpecificRecord specificRecord);
}
