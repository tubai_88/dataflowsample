package dataflowsample.utils;

import org.apache.avro.specific.SpecificRecord;
import org.apache.beam.sdk.transforms.SerializableFunction;

import com.google.api.services.bigquery.model.TableRow;

public class SampleSerializableFunction implements SerializableFunction<SpecificRecord, TableRow> {
	private static final long serialVersionUID = -6148443507989906830L;
	@Override
	public TableRow apply(SpecificRecord specificRecord) {
		return BigQueryAvroUtils.convertSpecificRecordToTableRow(
				specificRecord, BigQueryAvroUtils
						.getTableSchema(specificRecord.getSchema()));
	}
}
