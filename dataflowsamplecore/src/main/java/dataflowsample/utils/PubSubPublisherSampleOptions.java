package dataflowsample.utils;

import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.Validation.Required;

public interface PubSubPublisherSampleOptions extends BQInsertSampleOptions {
	@Description("Topic")
	@Required
	String getTopic();

	void setTopic(String value);

	
	@Description("DataType")
	@Required
	int getWindowSize();
	
	void setWindowSize(int value);
}
