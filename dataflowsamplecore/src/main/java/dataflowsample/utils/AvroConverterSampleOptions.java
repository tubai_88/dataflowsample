package dataflowsample.utils;

import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.Validation.Required;

public interface AvroConverterSampleOptions extends DataflowSampleOptions {
	@Description("Data type")
	@Required
	String getDatatype();

	void setDatatype(String value);
}
